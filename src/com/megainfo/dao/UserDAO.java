package com.megainfo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;

import com.megainfo.auth.DBConnection;
import com.megainfo.model.User;
import com.megainfo.util.DBUtil;
import com.megainfo.util.DateFormatter;

public class UserDAO {
	Connection con = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	Date today = new Date();
	public int registerUser(User user){
		int result = 0;
		String query = "INSERT INTO user_details VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		try{
			con = DBConnection.getConnection();
			ps = con.prepareStatement(query);
			ps.setString(1, user.getUsername());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getFirstname());
			ps.setString(4, user.getLastname());
			ps.setString(5, user.getDob());
			ps.setString(6, user.getGender());
			ps.setString(7, user.getPhone());
			ps.setString(8, user.getEmail());
			ps.setString(9, user.getAddress());
			ps.setString(10, user.getCity());
			ps.setString(11, user.getState());
			ps.setString(12, user.getCountry());
			ps.setString(13, user.getZipcode());
			ps.setString(14, user.getLast_login());
			
			result = ps.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			DBUtil.close(rs);
			DBUtil.close(ps);
			DBUtil.close(con);
		}
		
		return result;
	}
	
	public Boolean isUsernameExisting(User user){
		Boolean flag = false;
		String query = "SELECT * FROM user_details WHERE username = ?";
		try{
			con = DBConnection.getConnection();
			ps = con.prepareStatement(query);
			ps.setString(1, user.getUsername());
			rs = ps.executeQuery();
			if(rs.next()){
				flag=true;
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			DBUtil.close(rs);
			DBUtil.close(ps);
			DBUtil.close(con);
		}
		return flag;
	}
	public Boolean isValidUserLogin(User user){
		Boolean flag = false;
		String query = "SELECT * FROM user_details WHERE username = ? AND password = ?";
		try{
			con = DBConnection.getConnection();
			ps = con.prepareStatement(query);
			ps.setString(1, user.getUsername());
			ps.setString(2, user.getPassword());
			rs = ps.executeQuery();
			if(rs.next()){
				flag = true;
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			DBUtil.close(rs);
			DBUtil.close(ps);
			DBUtil.close(con);
		}
		return flag;
	}
	public void updateLastLogin() {
		DateFormatter df = new DateFormatter();
		String today_string = df.todayDateWithTime(today);
		String query = "UPDATE user_details SET last_login = ?";
		try{
			con = DBConnection.getConnection();
			ps = con.prepareStatement(query);
			ps.setString(1, today_string);
			ps.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			DBUtil.close(ps);
			DBUtil.close(con);
		}
	}
	public String loadLastLogin(String username){
		String last_login = null;
		String query = "SELECT last_login FROM user_details WHERE username = ?";
		try{
			con = DBConnection.getConnection();
			ps = con.prepareStatement(query);
			ps.setString(1, username);
			rs = ps.executeQuery();
			if(rs.next()){
				last_login = rs.getString("last_login");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return last_login;
	}
	public User loadUserDetails(User user){
		String query = "SELECT * FROM user_details WHERE username = ?";
		try{
			con = DBConnection.getConnection();
			ps = con.prepareStatement(query);
			ps.setString(1, user.getUsername());
			rs = ps.executeQuery();
			if(rs.next()){
				user = new User();
				user.setUsername(rs.getString("username"));
				user.setAddress(rs.getString("address"));
				user.setCity(rs.getString("city"));
				user.setCountry(rs.getString("country"));
				user.setDob(rs.getString("dob"));
				user.setEmail(rs.getString("email"));
				user.setFirstname(rs.getString("firstname"));
				user.setGender(rs.getString("gender"));
				user.setLastname(rs.getString("lastname"));
				user.setPassword(rs.getString("password"));
				user.setPhone(rs.getString("phone"));
				user.setState(rs.getString("state"));
				user.setZipcode(rs.getString("zipcode"));
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			DBUtil.close(rs);
			DBUtil.close(ps);
			DBUtil.close(con);
		}
		
		return user;
	}
	public int updateUserProfile(User user){
		int result = 0;
		String query = "UPDATE user_details SET firstname=?, lastname=?, dob=?, phone=?, email=?, address=?, city = ?, "
				+ "state=?, country=?, zipcode=?";
		try{
			con = DBConnection.getConnection();
			ps = con.prepareStatement(query);
			ps.setString(1, user.getFirstname());
			ps.setString(2, user.getLastname());
			ps.setString(3, user.getDob());
			ps.setString(4,  user.getPhone());
			ps.setString(5, user.getEmail());
			ps.setString(6, user.getAddress());
			ps.setString(7, user.getCity());
			ps.setString(8, user.getState());
			ps.setString(9, user.getCountry());
			ps.setString(10, user.getZipcode());
			
			result = ps.executeUpdate();
			
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			DBUtil.close(ps);
			DBUtil.close(con);
		}
		return result;
	}
}

-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 29, 2023 at 01:23 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart_details`
--

CREATE TABLE `cart_details` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `product_id` varchar(30) NOT NULL,
  `added_on` varchar(30) NOT NULL,
  `is_product_ordered` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart_details`
--

INSERT INTO `cart_details` (`id`, `username`, `product_id`, `added_on`, `is_product_ordered`) VALUES
(1, 'rose', 'C1', '12/12/2016', 0),
(42, 'rose', 'M100', '29/05/2023 16:16', 1);

-- --------------------------------------------------------

--
-- Table structure for table `count_details`
--

CREATE TABLE `count_details` (
  `id` int(11) NOT NULL,
  `product_id` varchar(30) NOT NULL,
  `username` varchar(30) NOT NULL,
  `view_count` int(11) DEFAULT NULL,
  `view_date` varchar(30) DEFAULT NULL,
  `add_to_cart_count` int(11) DEFAULT 0,
  `add_to_cart_date` varchar(30) DEFAULT NULL,
  `order_count` int(11) DEFAULT 0,
  `order_date` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `count_details`
--

INSERT INTO `count_details` (`id`, `product_id`, `username`, `view_count`, `view_date`, `add_to_cart_count`, `add_to_cart_date`, `order_count`, `order_date`) VALUES
(1, 'C1', 'rose', 4, '15/03/2017 12:57', 0, '', 0, NULL),
(16, 'M100', 'rose', 1, '29/05/2023 16:16', 1, '29/05/2023 16:16', 1, '29/05/2023 16:16');

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `order_id` int(11) NOT NULL,
  `ordered_product_id` varchar(30) NOT NULL,
  `ordered_product_units` int(11) NOT NULL,
  `total_order_amount` decimal(10,2) NOT NULL,
  `ordered_on` varchar(30) NOT NULL,
  `ordered_by` varchar(30) NOT NULL,
  `order_status` varchar(30) NOT NULL,
  `order_status_updated_on` varchar(30) NOT NULL,
  `is_order_delivered` tinyint(1) NOT NULL,
  `order_delivered_on` varchar(30) NOT NULL,
  `is_order_cancelled` tinyint(1) NOT NULL,
  `order_cancelled_on` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`order_id`, `ordered_product_id`, `ordered_product_units`, `total_order_amount`, `ordered_on`, `ordered_by`, `order_status`, `order_status_updated_on`, `is_order_delivered`, `order_delivered_on`, `is_order_cancelled`, `order_cancelled_on`) VALUES
(1, 'C1', 2, '700.00', '14/03/2017 13:20', 'rose', 'Pending', '14/03/2017 13:20', 0, '', 0, ''),
(10, 'M100', 2, '110.00', '29/05/2023 16:16', 'rose', 'Pending', '29/05/2023 16:16', 0, '', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `product_details`
--

CREATE TABLE `product_details` (
  `product_id` varchar(30) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `product_category` varchar(50) NOT NULL,
  `product_unit_price` decimal(10,2) NOT NULL,
  `product_description` varchar(200) NOT NULL,
  `product_registered_on` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_details`
--

INSERT INTO `product_details` (`product_id`, `product_name`, `product_category`, `product_unit_price`, `product_description`, `product_registered_on`) VALUES
('100', 'Center Fresh', 'food', '5.00', 'Refreshing chewing gum', '13/03/2017 21:51'),
('111', 'Axe Deodorant', 'cosmetics', '350.00', 'Body Perfume', '13/03/2017 21:52'),
('12300', 'Vaseline', 'cosmetics', '55.00', '100% Pure Petroleum Jelly', '15/03/2017 10:15'),
('1234', 'Ear Phone', 'electronics', '500.00', 'Skullcandy Head Phone', '10/03/2017 14:43'),
('12345', 'Mobile Charger', 'electronics', '1000.00', 'Sony Xperia Mobile Charger.', '10/03/2017 16:38'),
('22', 'Lays', 'food', '20.00', 'Lays American Cheese Cream', '13/03/2017 12:21'),
('78io', 'Dell Inspiron Laptop', 'electronics', '45000.00', 'Dell Inspiron 15R Laptop for personal use', '16/03/2017 14:44'),
('8blpo', 'Dining Table', 'furniture', '12000.00', 'Dining table for 4 persons.', '16/03/2017 14:46'),
('ABC111', 'Bailey', 'food', '20.00', 'Packaged Drinking Water', '10/03/2017 12:05'),
('abc123', 'Product', 'electronics', '1000.00', 'Electronic', '10/03/2017 12:08'),
('C1', 'Hair Shampoo', 'cosmetics', '200.00', 'Dove Hairfall Control Hair Shampoo', '10/03/2017 13:13'),
('C122', 'Coca Cola', 'food', '75.00', '2 Ltrs Cold Drinks', '15/03/2017 13:35'),
('KitKat', 'Kit Kat Chocolate', 'food', '10.00', 'Kit Kat Chocolate', '29/05/2023 16:17'),
('L010', 'Lays', 'food', '60.00', 'Jumbo spanish tomato flavour lays for family', '16/03/2017 14:22'),
('M100', 'Maaza', 'food', '55.00', 'Maaza Mango Drink', '15/03/2017 14:52'),
('M89', 'Kit Kat', 'food', '25.00', 'Kitkat chocolate wraffers', '16/03/2017 14:45'),
('P12', 'Pepsi', 'food', '70.00', '2 Ltrs. Cold Drink', '15/03/2017 13:56'),
('tv1', 'Sony BRAVIA 4K', 'electronics', '120000.00', 'Discover Sony 4K HDR picture quality and experience stunning clarity, colour and contrast.All on a beautifully slim, elegant TV with slice of living design.Sony\'s Android TV further expands the conten', '15/03/2017 13:05');

-- --------------------------------------------------------

--
-- Table structure for table `product_rating`
--

CREATE TABLE `product_rating` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` varchar(30) NOT NULL,
  `product_rating` int(10) UNSIGNED NOT NULL,
  `review_title` varchar(100) NOT NULL,
  `review_message` varchar(500) NOT NULL,
  `rated_by` varchar(30) NOT NULL,
  `rated_on` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_rating`
--

INSERT INTO `product_rating` (`id`, `product_id`, `product_rating`, `review_title`, `review_message`, `rated_by`, `rated_on`) VALUES
(1, 'C1', 4, 'Very Good Product', 'Product brand and quality is awesome.', 'rose', '27/03/2017 12:46');

-- --------------------------------------------------------

--
-- Table structure for table `search_count_details`
--

CREATE TABLE `search_count_details` (
  `search_id` int(10) UNSIGNED NOT NULL,
  `username` varchar(30) NOT NULL,
  `category` varchar(30) DEFAULT NULL,
  `category_search_count` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `recent_category_search_date` varchar(30) DEFAULT NULL,
  `product` varchar(100) DEFAULT NULL,
  `product_search_count` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `recent_product_search_date` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `search_count_details`
--

INSERT INTO `search_count_details` (`search_id`, `username`, `category`, `category_search_count`, `recent_category_search_date`, `product`, `product_search_count`, `recent_product_search_date`) VALUES
(1, 'rose', 'food', 5, '16/03/2017 14:17', NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `dob` varchar(30) NOT NULL,
  `gender` varchar(30) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `city` varchar(30) NOT NULL,
  `state` varchar(30) NOT NULL,
  `country` varchar(30) NOT NULL,
  `zipcode` varchar(10) NOT NULL,
  `last_login` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`username`, `password`, `firstname`, `lastname`, `dob`, `gender`, `phone`, `email`, `address`, `city`, `state`, `country`, `zipcode`, `last_login`) VALUES
('rose', 'tiger', 'Rose', 'K', '1990-05-04', 'Male', '1234567890', 'rose@gmail.com', 'Vijayanagar', 'Bangalore', 'Karnataka', 'India', '560085', '29/05/2023 16:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart_details`
--
ALTER TABLE `cart_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `count_details`
--
ALTER TABLE `count_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `product_details`
--
ALTER TABLE `product_details`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `product_rating`
--
ALTER TABLE `product_rating`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `search_count_details`
--
ALTER TABLE `search_count_details`
  ADD PRIMARY KEY (`search_id`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart_details`
--
ALTER TABLE `cart_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `count_details`
--
ALTER TABLE `count_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `product_rating`
--
ALTER TABLE `product_rating`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `search_count_details`
--
ALTER TABLE `search_count_details`
  MODIFY `search_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
